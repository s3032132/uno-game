# UNO Multiplayer Java game

This is the repository of the UNO multiplayer game by Iulia Costea and Wouter Deen. It is written in Java and the provided JAR file can be used for running a server or a client.

## Java version
Please use Java version 17 and up to run the provided JAR file (class version 61).

## Running a server
Open (double-click) the provided _server.bat_ file in Windows File Explorer (not IntelliJ), or use this command:
```
java -jar -Xss1m uno-game.jar server <port>
```
This command will run a new server that will be listening on the specified port number. For example, to start a game running on port 1234, you can run `java -jar -Xss1m uno-game.jar server 1234`.  

## Running a client
Open (double-click) the provided _server.bat_ file in Windows File Explorer (not IntelliJ), or use this command:
```
java -jar uno-game.jar client
```
To connect to a server running on the same computer, you can just enter `localhost` as a hostname. Other hostnames, except IPv4 addresses containing 3 dots, will be regarded as invalid.

### Unicode characters displaying incorrectly
If you are using the new Windows Terminal on Windows 11, executing the code manually (not using the .bat files), and unicode characters are not displaying correctly, please type `chcp 650010` into the terminal and try again. This will set the correct charset. If you are not using the new Windows Terminal, but rather, the old Windows 10 Command Prompt, you might want to run the code directly inside of IntelliJ instead of running the provided .bat files.


## Commands
You can type `/help` in the client to see all available commands.

| Command                    | Description                                                            |
|----------------------------|------------------------------------------------------------------------|
| /join \<gameSize\>         | Join a game with _gameSize_ player slots.                              |
| /computergame \<gameSize\> | Starts a game with _gameSize_ player slots with only computer players. |
| /play \<cardNumber\>       | Play the card of the index _cardNumber_ in your hand.                  |
| /draw                      | Request to draw a card from the deck.                                  |
| /direction                 | View the direction of the game (clockwise/counter-clockwise)           |
| /topcard                   | View the top card on the discard pile.                                 |
| /hand or /cards            | View the cards that you are holding in your hand.                      |
| /help                      | See all available commands.                                            |

## Computer players
In our UNO game, we have implemented computer players on the server. This means that if you join a game of 4 players, and 1 other player joins, the remaining two player slots will be filled by computer players.
### Game with only computer players
To start a game with only computer players, first start a server, then a client. You can enter a nickname for the client, but that does not matter. After that, type `/computergame <gameSize>` in the client application terminal. This starts a game with only computer players.