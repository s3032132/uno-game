package nl.utwente.cardgame.Controller.Client;

import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.CardPile.Hand;
import nl.utwente.cardgame.Model.GameDirection;
import nl.utwente.cardgame.View.InputHandler;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Making the initial connection to the server.
 */
public class Client {
  private Socket client;
  private BufferedReader in;
  private PrintWriter out;
  private boolean connected;
  private ArrayList<String> players;
  private int gameSize;
  private InputHandler input;
  private String displayName;
  private Hand hand;
  private GameDirection gameDirection;
  private Card buffer;
  private boolean isInGame;
  private JSONObject topCard;
  private boolean computerGame;

  public Client() {
    connected = true;
    players = new ArrayList<>();
    input = InputHandler.getInstance();
    hand = new Hand();
    gameDirection = GameDirection.CLOCKWISE;
  }
  
  public void run() {
    input.printLine("Starting uno...");
    input.clear();

    try {
      while (client == null) {
        try {
          client = new Socket(input.getHost(), input.getPort());
        } catch (IOException e) {
          input.clear();
          input.printError("Could not find an UNO server running on this port and IP address.");
        }
      }

      out = new PrintWriter(client.getOutputStream(), true);
      in = new BufferedReader(new InputStreamReader(client.getInputStream()));

      OutgoingHandler outHandler = new OutgoingHandler(this, out);
      Thread t = new Thread(outHandler);
      t.start();

      // Listen for messages from the server
      IncomingHandler inHandler = new IncomingHandler(this, in);
      inHandler.run();

    } catch (IOException e) {
      input.printError("The server disconnected.");
      shutdown();
    }
  }

  public void shutdown() {
    connected = false;
    try {
      in.close();
      out.close();
      if(!client.isClosed()) {
        client.close();
      }
    } catch (IOException e) {
      // ignore
    }
  }

  public boolean isConnected() {
    return connected;
  }

  public ArrayList<String> getPlayers() {
    return players;
  }

  public void setGameSize(int gameSize) {
    this.gameSize = gameSize;
  }

  public int getGameSize() {
    return gameSize;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public Hand getHand() {
    return hand;
  }

  public GameDirection getGameDirection() {
    return gameDirection;
  }

  public void setGameDirection(GameDirection gameDirection) {
    this.gameDirection = gameDirection;
  }

  public void setBuffer(Card card){
    buffer=card;
  }

  public Card getBuffer(){
    return buffer;
  }

  public JSONArray handAsJSONArray() {
    JSONArray jCards = new JSONArray();
    for (Card card : hand.getCards()) {
      JSONObject jCard = new JSONObject();
      jCard.put("color", card.getColor());
      jCard.put("value", card.getValue());
      jCards.put(jCard);
    }
    return jCards;
  }

  public void setInGame(boolean inGame) {
    isInGame = inGame;
  }

  public boolean isInGame() {
    return isInGame;
  }

  public void setTopCard(JSONObject topCard) {
    this.topCard = topCard;
  }

  public JSONObject getTopCard() {
    return topCard;
  }

  public void setComputerGame(boolean computerGame) {
    this.computerGame = computerGame;
  }

  public boolean isComputerGame() {
    return computerGame;
  }
}
