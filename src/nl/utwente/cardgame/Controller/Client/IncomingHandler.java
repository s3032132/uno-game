package nl.utwente.cardgame.Controller.Client;

import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.GameDirection;
import nl.utwente.cardgame.Model.NetworkProtocol.CardValue;
import nl.utwente.cardgame.Model.NetworkProtocol.Command;
import nl.utwente.cardgame.Model.NetworkProtocol.Error;
import nl.utwente.cardgame.Model.Utils.JSON;
import nl.utwente.cardgame.View.InputHandler;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class IncomingHandler {
  BufferedReader serverIn;
  private final InputHandler input;
  private final Client client;
  private String currentPlayerName;
  private final HashMap<String, Integer> playerCardCount;

  public IncomingHandler(Client client, BufferedReader in) {
    this.client = client;
    this.serverIn = in;
    input = InputHandler.getInstance();
    currentPlayerName = "";
    playerCardCount = new HashMap<>();
  }

  public void run() {
    String inMessage;
    try {
      while((inMessage = serverIn.readLine()) != null) {
        JSONObject jo = new JSONObject(inMessage);
        Command cmd = jo.getEnum(Command.class, "command");

        switch (cmd) {
          case QUEUE_JOINED, QUEUE_LEFT -> {
            client.setInGame(true);
            client.getPlayers().clear();
            JSONArray playerNames = jo.getJSONArray("playerNames");
            ArrayList<String> temp = new ArrayList<>();
            for (int i = 0; i < playerNames.length(); i++) {
              if (!client.isComputerGame() || !playerNames.getString(i).equals(client.getDisplayName())) {
                temp.add(playerNames.getString(i));
              }
            }
            client.getPlayers().addAll(temp);

            jo.put("playerCount", client.getPlayers().size());
            jo.put("gameSize", client.getGameSize());
            input.print(jo);
          }
          case ROUND_STARTED -> {
            input.print(jo);
            client.setGameDirection(GameDirection.CLOCKWISE);
            ArrayList<Card> cards = JSON.getInstance().ArrayToCards(jo, "initialCards");
            for (Card card : cards) client.getHand().addCard(card);
            for (String player : client.getPlayers()) {
              playerCardCount.put(player, 7);
            }
          }
          case NEXT_TURN -> {
            jo.put("thisPlayerName", client.getDisplayName());
            jo.put("prevPlayerName", currentPlayerName);
            jo.put("cardsInHand", client.handAsJSONArray());
            input.print(jo);

            if (jo.has("playedCard")) {
              JSONObject playedCard = jo.getJSONObject("playedCard");
              client.setTopCard(playedCard);
              CardValue value = playedCard.getEnum(CardValue.class, "value");
              if (!currentPlayerName.equals("")) {
                playedCard.put(currentPlayerName, playerCardCount.get(currentPlayerName) - 1);
              }
              if (value == CardValue.R) {
                if (client.getGameDirection() == GameDirection.CLOCKWISE) {
                  client.setGameDirection(GameDirection.COUNTER_CLOCKWISE);
                } else client.setGameDirection(GameDirection.CLOCKWISE);
              }
            }

            if (jo.has("displayName")) {
              currentPlayerName = (jo.getString("displayName"));
            }
          }
          case ISSUE_CARD -> {
            input.print(jo);
            ArrayList<Card> cards = JSON.getInstance().ArrayToCards(jo, "card");
            for (Card card : cards) client.getHand().addCard(card);
          }
          case ROUND_FINISHED -> {
            input.print(jo);
            client.getHand().clearPile();
            currentPlayerName = "";
          }
          case GAME_FINISHED -> {
            input.print(jo);
            client.setInGame(false);
            client.getHand().clearPile();
            client.setComputerGame(false);
          }
          case ERROR -> {
            Error code = jo.getEnum(Error.class, "code");
            switch (code) {
              case INVALID_NAME -> {
                input.printLine("Please enter a new name:");
                client.setDisplayName(null);
              }
              case PLAYER_DISCONNECTED -> {
                input.printLine("You can join a new game by typing '/join <gameSize'.");
                client.setInGame(false);
                client.getHand().clearPile();
                client.setComputerGame(false);
              }
              case INVALID_MOVE -> {
                jo.put("cardsInHand", client.handAsJSONArray());
                if (client.getBuffer() != null) {
                  client.getHand().addCard(client.getBuffer());
                  client.setBuffer(null);
                }
                input.printLine("Type '/cards' to see your cards.");
              }
            }
            input.print(jo);
          }
          default -> input.print(jo);
        }
      }
    } catch (IOException e) {
      if (e.getMessage().equals("Connection reset")) {
        input.printError("The server has been disconnected.");
      } else {
        input.printError("Failed to parse a message received from the server.");
        e.printStackTrace();
      }
    }
  }
}
