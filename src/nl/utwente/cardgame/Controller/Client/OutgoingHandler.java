package nl.utwente.cardgame.Controller.Client;

import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.NetworkProtocol.CardValue;
import nl.utwente.cardgame.Model.NetworkProtocol.Command;
import nl.utwente.cardgame.Model.NetworkProtocol.Functionality;
import nl.utwente.cardgame.View.InputHandler;
import org.json.JSONObject;

import java.io.PrintWriter;

public class OutgoingHandler implements Runnable {
  private Client client;
  private InputHandler input;
  private final String PROTOCOL_VERSION = "1.2";
  private final Functionality[] FUNCTIONALITIES;
  private PrintWriter out;

  public OutgoingHandler(Client client, PrintWriter out) {
    this.client = client;
    FUNCTIONALITIES = new Functionality[]{Functionality.CHAT, Functionality.LOBBY, Functionality.MULTI_GAME};
    input = InputHandler.getInstance();
    this.out = out;
  }

  @Override
  public void run() {
    while (client.isConnected()) {
      if (client.getDisplayName() == null) {
        String inputDisplayName = input.getDisplayName();
        input.clear();
        handleDisplayName(inputDisplayName);
      } else {
        handleCommands();
      }
    }
  }

  private void handleCommands() {
    JSONObject jo = new JSONObject();
    String msg = input.readLine();
    String[] args = msg.split(" ");

    if (client.getDisplayName() == null) {
      handleDisplayName(msg);
    } else {
      switch (args[0]) {
        case "/join" -> {
          if(!client.isInGame()) {
            if(args.length == 2) {
              try {
                int gameSize = Integer.parseInt(args[1]);
                if(gameSize < 2 || gameSize > 10) throw new Exception();
                input.clear();
                client.setGameSize(gameSize);
                jo.put("command", Command.JOIN_GAME).put("gameSize", gameSize);
                out.println(jo);
              } catch (Exception e) {
                input.printError("Please enter a valid number of players that you want to play a game with.");
              }
            } else {
              input.printError("Please type: '/join <gameSize>', where gameSize is a number.");
            }
          } else {
            input.printError("You're already in a game. You can leave the game by closing the client.");
          }
        }
        case "/play" -> {
          if (!client.isInGame()) break;
          if (args.length == 2) {
            try {
              int cardNum = Integer.parseInt(args[1]);
              JSONObject jCard = new JSONObject();
              if (cardNum < client.getHand().getCards().size() && cardNum >= 0) {
                Card card = client.getHand().getCards().get(cardNum);
                CardValue value = card.getValue();
                CardColor color = null;
                jCard.put("value", value);

                if(value == CardValue.W || value == CardValue.F) {
                  while (color == null) {
                    String inputColor = input.getWildcardColor();
                    switch (inputColor) {
                      case "red" -> color = CardColor.R;
                      case "yellow" -> color = CardColor.Y;
                      case "green" -> color = CardColor.G;
                      case "blue" -> color = CardColor.B;
                      default -> input.printError("Invalid card color.");
                    }
                  }
                } else {
                  color = card.getColor();
                }

                client.setBuffer(card);
                client.getHand().getCards().remove(cardNum);

                jCard.put("color", color);
                jo.put("command", Command.PLAY_CARD);
                jo.put("card", jCard);
                out.println(jo);

                input.clear();
              } else {
                input.printError("Please input a valid card number.");
              }
            } catch (NumberFormatException e) {
              input.printError("Could not parse your input to a number. Please type '/play <cardNumber>'" +
                      " or type '/help' for help.");
            }
          } else {
            input.printError("Please type: 'play <cardIndex>', where card index is the number in front of the card.");
          }
        }
        case "/direction" -> input.printLine("The current game direction is " + client.getGameDirection() + ".");
        case "/draw" -> {
          if (!client.isInGame()) break;
          jo.put("command", Command.DRAW_CARD);
          input.clear();
          out.println(jo);
        }
        case "/topcard" -> {
          if (!client.isInGame()) break;
          if (client.getTopCard() != null) {
            CardColor color = client.getTopCard().getEnum(CardColor.class, "color");
            CardValue value = client.getTopCard().getEnum(CardValue.class, "value");
            input.printLine("The top card on the discard pile has " + input.colorPrettier(color)
                    + " and value " + input.valuePrettier(value));
          } else {
           input.printError("There is no top card on the discard pile.");
          }
        }
        case "/help" -> input.printHelp();
        case "/computergame" -> {
          if(!client.isInGame()) {
            if(args.length == 2) {
              try {
                int gameSize = Integer.parseInt(args[1]);
                if(gameSize < 2 || gameSize > 10) throw new Exception();
                client.setGameSize(gameSize);
                client.setComputerGame(true);
                jo.put("command", Command.JOIN_GAME).put("gameSize", gameSize);
                jo.put("onlyComputerPlayers", true);
                out.println(jo);
              } catch (Exception e) {
                input.printError("Please enter a valid number of players that you want to play a game with.");
              }
            } else {
              input.printError("Please type: '/computergame <gameSize>', where gameSize is a number.");
            }
          } else {
            input.printError("You're already in a game. You can leave the game by closing the client.");
          }
        }
        case "/hand", "/cards" -> input.printJSONCardsArray(client.handAsJSONArray());
        default -> {
          if (msg.startsWith("/")) {
            input.printError("Please type a valid command.");
          } else {
            jo.put("command", Command.CHAT);
            jo.put("message", msg);
            out.println(jo);
          }
        }
      }
    }
  }

  private void handleDisplayName(String name) {
    JSONObject jo = new JSONObject();

    if (name.length() > 0) {
      client.setDisplayName(name);
      jo.put("command", Command.HI)
              .put("displayName", name)
              .put("protocolVersion", PROTOCOL_VERSION)
              .put("functionalities", FUNCTIONALITIES);
      out.println(jo);
    } else {
      input.printLine("Please enter a valid nickname.");
    }
  }
}
