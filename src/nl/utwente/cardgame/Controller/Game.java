package nl.utwente.cardgame.Controller;

import nl.utwente.cardgame.Model.Card.ActionCard;
import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.Card.CardActions.WildAction;
import nl.utwente.cardgame.Model.Card.CardActions.WildDrawAction;
import nl.utwente.cardgame.Model.CardPile.DiscardPile;
import nl.utwente.cardgame.Model.CardPile.DrawPile;
import nl.utwente.cardgame.Model.Exceptions.MaxPlayersReachedException;
import nl.utwente.cardgame.Model.Exceptions.PlayerAlreadyInGameException;
import nl.utwente.cardgame.Model.GameDirection;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.NetworkProtocol.Command;
import nl.utwente.cardgame.Model.NetworkProtocol.Error;
import nl.utwente.cardgame.Model.Player.ComputerPlayer;
import nl.utwente.cardgame.Model.Player.HumanPlayer;
import nl.utwente.cardgame.Model.Player.Player;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Game implements Runnable {
  private final ArrayList<Player> players;
  private DrawPile drawPile;
  private final DiscardPile discardPile;
  private Player currentPlayer;
  private GameDirection direction;
  private CardColor currentColor;
  private boolean onlyComputerPlayers;
  public int starterCardAmount;

  /**
   * Constructs a new Game object. The game does not contain any players by default.
   */
  public Game(int starterCardAmount, boolean onlyComputerPlayers) {
    this.starterCardAmount = starterCardAmount;
    players = new ArrayList<>();
    drawPile = new DrawPile(this);
    discardPile = new DiscardPile(drawPile);
    direction = GameDirection.CLOCKWISE;
    this.onlyComputerPlayers = onlyComputerPlayers;
  }

  /**
   * Adds a player to the game.
   * @param p
   * @throws PlayerAlreadyInGameException - If the player has already joined the game.
   */
  public void addPlayer(Player p) throws PlayerAlreadyInGameException, MaxPlayersReachedException {
    if ((!onlyComputerPlayers && players.size() == 10) || (onlyComputerPlayers && players.size() == 11)) {
      throw new MaxPlayersReachedException("Too many players have joined the game");
    }
    if (!players.contains(p)) {
      players.add(p);
    } else throw new PlayerAlreadyInGameException("Player " + p.getName() +  " has already joined the game.");
  }

  /**
   * Will set the player to be the winner of this game.
   */
  public void finish() {
    JSONObject jo = new JSONObject();
    jo.put("command", Command.GAME_FINISHED);
    JSONObject scoreboard = new JSONObject();

    for (Player player : players) {
      if (!onlyComputerPlayers || player instanceof ComputerPlayer) {
        scoreboard.put(player.getName(), player.getPoints());
      }
    }

    jo.put("scoreboard", scoreboard);
    broadcast(jo);
    shutdown();
  }

  /**
   * Gets all the active players in the game (excluding the winners).
   * @return The payers in the game.
   */
  public ArrayList<Player> getPlayers() {
    return players;
  }

  public Player getPlayer(String displayName) {
    for (Player p : players) {
      if (p.getName().equals(displayName)) return p;
    }
    return null;
  }

  /**
   * Gets the player whose turn it is now.
   * @return The current player.
   */
  public Player getCurrentPlayer() {
    return currentPlayer;
  }

  /**
   * Gets the player whose turn it is next (after the current player).
   * This does <u>not</u> change the current player.
   * The next player depends upon the current <code>GameDirection</code> (clockwise or counter-clockwise).
   * @return The next player.
   */
  public Player getNextPlayer() {
    int nextPlayerIndex;

    if(direction == GameDirection.CLOCKWISE) {
      nextPlayerIndex = players.indexOf(currentPlayer) + 1;
      if (nextPlayerIndex == players.size()){
        nextPlayerIndex = 0;
      }
    } else {
      nextPlayerIndex = players.indexOf(currentPlayer) - 1;
      if (nextPlayerIndex == -1){
        nextPlayerIndex = players.size() - 1;
      }
    }

    Player nextPlayer = players.get(nextPlayerIndex);

    if (onlyComputerPlayers && nextPlayer instanceof HumanPlayer) {
      int nextComputerPlayerIndex;

      if(direction == GameDirection.CLOCKWISE) {
        nextComputerPlayerIndex = nextPlayerIndex + 1;
        if (nextComputerPlayerIndex == players.size()){
          nextComputerPlayerIndex = 0;
        }
      } else {
        nextComputerPlayerIndex = nextPlayerIndex - 1;
        if (nextComputerPlayerIndex == -1){
          nextComputerPlayerIndex = players.size() - 1;
        }
      }

      nextPlayer = players.get(nextComputerPlayerIndex);
    }

    return nextPlayer;
  }

  /**
   * Gives the turn to the next player and returns that player.
   * @return The new current player.
   */
  public Player nextPlayer() {
    currentPlayer = getNextPlayer();
    return currentPlayer;
  }

  /**
   * Gets the deck with all the cards that have not been played yet.
   * @return The deck.
   */
  public DrawPile getDrawPile() {
    return drawPile;
  }

  /**
   * Reconstructs the <code>DrawPile</code>. This reinstates it will all the possible cards in the game.
   */
  public void resetDrawPile() {
    drawPile = new DrawPile(this);
  }

  /**
   * Gets the discard pile that contains all the cards that have been played.
   * @return The discard pile.
   */
  public DiscardPile getDiscardPile() {
    return discardPile;
  }

  /**
   * Gets the current color of the game's cards.
   * @return The current color.
   */
  public CardColor getCurrentColor() {
    return currentColor;
  }

  /**
   * Sets the current color of the game's cards.
   * @param color The new color.
   */
  public void setCurrentColor(CardColor color) {
    this.currentColor = color;
  }

  /**
   * Sets the current player to the specified player.
   * @param player The new current player.
   */
  public void setCurrentPlayer(Player player) {
    this.currentPlayer = player;
  }

  /**
   * Switches the game's direction from clockwise to counter-clockwise or vice versa.
   */
  public void switchDirection() {
    if(direction == GameDirection.CLOCKWISE) {
      direction = GameDirection.COUNTER_CLOCKWISE;
    } else {
      direction = GameDirection.CLOCKWISE;
    }
  }

  public GameDirection getDirection() {
    return direction;
  }

  /**
   * Executes if the <code>PLAY_CARD</code> command is received. It will first check if it's the players turn and if
   * the card is a valid card to play. After this, it will make the player play the card and broadcast the
   * <code>NEXT_TURN</code> command to all players in the game.
   *
   * This method also calls the player's <code>calculatePoints()</code> method, if the player has no cards left.
   *
   * If the next player is a computer player, this method calls <code>handleComputerPlayer()</code>. This call all
   * necessary methods for a computer player to select and play or draw a card.
   *
   * If the card is invalid or if it's not the player's turn, this method will send an <code>INVALID_MOVE</code>
   * over the network to the caller.
   *
   * @param card The card to play.
   * @param p The player that requests to play this card.
   */
  public void playCard(Card card, Player p) {
    if(currentPlayer == p) {
      if (card.isValidMove(this)) {
        p.playCard(this, card);

        JSONObject nextTurnCmd = new JSONObject();
        nextTurnCmd.put("command", Command.NEXT_TURN);
        nextTurnCmd.put("prevPlayerCardCount", currentPlayer.getHand().getCards().size());

        JSONObject jCard = new JSONObject();
        jCard.put("color", card.getColor());
        jCard.put("value", card.getValue());

        nextTurnCmd.put("playedCard", jCard);

        nextPlayer();

        if (p.getHand().getCards().size() == 0) {
          broadcast(nextTurnCmd);
          p.calculatePoints(this);
        } else {
          nextTurnCmd.put("displayName", currentPlayer.getName());
          broadcast(nextTurnCmd);
          handleComputerPlayer();
        }
      } else {
        JSONObject error = new JSONObject();
        error.put("command", Command.ERROR);
        error.put("code", Error.INVALID_MOVE);
        error.put("description", "This is an invalid move.");
        if (p instanceof HumanPlayer) ((HumanPlayer) p).getHandler().sendObject(error);
      }
    } else {
      JSONObject error = new JSONObject();
      error.put("command", Command.ERROR);
      error.put("code", Error.INVALID_MOVE);
      error.put("description", "It is not your turn.");
      if (p instanceof HumanPlayer) ((HumanPlayer) p).getHandler().sendObject(error);
    }
  }
  /**
   * Executes if the <code>DRAW_CARD</code> command is received.<br/><br/>
   *
   * It will first check if it's the players turn. After this, it will make the player draw the card and broadcast the
   * <code>NEXT_TURN</code> command to all players in the game without a specified card. It will also issue the card
   * to the client over the network via the <code>ISSUE_CARD</code> command.<br/><br/>
   *
   * If the next player is a computer player, this method calls <code>handleComputerPlayer()</code>. This call all
   * necessary methods for a computer player to select and play or draw a card.<br/><br/>
   *
   * If it's not the player's turn, this method will send an <code>INVALID_MOVE</code> over the network to the caller.<br/><br/>
   *
   * @param p The player that requests to draw a card.
   */
  public void drawCard(Player p) {
    if (currentPlayer == p) {
      Card card = currentPlayer.drawCard(drawPile);

      JSONObject jCard = new JSONObject();
      jCard.put("color", card.getColor());
      jCard.put("value", card.getValue());

      JSONObject nextTurnCmd = new JSONObject();
      nextTurnCmd.put("command", Command.NEXT_TURN);
      nextTurnCmd.put("prevPlayerCardCount", currentPlayer.getHand().getCards().size());

      if (card.isValidMove(this)
              && card instanceof ActionCard
              && !(((ActionCard) card).getAction() instanceof WildAction)
              && !(((ActionCard) card).getAction() instanceof WildDrawAction)
      ) {
        p.playCard(this, card);
        nextTurnCmd.put("playedCard", jCard);
        JSONObject chat = new JSONObject();
        chat.put("command", Command.CHAT);
        chat.put("message", currentPlayer.getName() + " drew a card. Since this card was immediately playable, they " +
                "also played this card in the same turn!");
        chat.put("displayName", "server");
        broadcast(chat);
      } else if (p instanceof HumanPlayer) {
        JSONObject issueCardCmd = new JSONObject();
        issueCardCmd.put("command", Command.ISSUE_CARD);
        JSONArray cardsToIssue = new JSONArray().put(jCard);
        issueCardCmd.put("card", cardsToIssue);
        ((HumanPlayer) p).getHandler().sendObject(issueCardCmd);
      }

      nextPlayer();
      nextTurnCmd.put("displayName", currentPlayer.getName());

      if (!onlyComputerPlayers || currentPlayer instanceof ComputerPlayer) {
        broadcast(nextTurnCmd);
      }

      try {
        handleComputerPlayer();
      } catch (StackOverflowError e) {
        System.out.println("We're sorry, our computer player is too dumb to win." +
                " This resulted in a stackoverflow error. Please increase your stack size try again.");
        throw e;
      }
    } else {
      JSONObject error = new JSONObject();
      error.put("command", Command.ERROR);
      error.put("code", Error.INVALID_MOVE);
      error.put("description", "It is not your turn.");
      if (p instanceof HumanPlayer) ((HumanPlayer) p).getHandler().sendObject(error);
    }
  }

  /**
   * Calls all necessary functions to handle a computer player's turn. It will make the computer player select a
   * card to play by invoking the <code>playCard()</code> method in the Game class. If the computer player can't play a
   * card, it will make the computer player draw a card by invoking the <code>drawCard()</code> method in the Game class.
   */
  private void handleComputerPlayer() {
    if (currentPlayer instanceof ComputerPlayer) {
      Card card = ((ComputerPlayer) currentPlayer).selectCardToPlay(this);
      if (card == null) {
        drawCard(currentPlayer);
      } else {
        playCard(card, currentPlayer);
      }
    }
  }

  /**
   * <b>Starts the game.</b> <br>
   * First every player gets 7 random cards in their hand from the deck.
   * Asks after each game whether the admin of the game wants to continue playing.
   * Continues until the admin does not want to play anymore.
   */
  @Override
  public void run() {
    initiateGame();
  }

  public void initiateGame() {
    JSONObject roundStartedCmd = new JSONObject();
    roundStartedCmd.put("command", Command.ROUND_STARTED);

    // Issue 7 cards to each player.
    for(Player p : players) {
      if (!onlyComputerPlayers || p instanceof ComputerPlayer) {
        for(int i = 0; i < starterCardAmount; i++) {
          p.drawCard(drawPile);
        }
      }

      roundStartedCmd.put("initialCards", p.getHand().getCards());
      if(p instanceof HumanPlayer) {
        ((HumanPlayer) p).getHandler().sendObject(roundStartedCmd);
      }
    }

    while (currentPlayer == null || (onlyComputerPlayers && currentPlayer instanceof HumanPlayer)) {
      // Select random player to be the first player.
      int randomIndex = (int) (Math.random() * players.size());
      currentPlayer = players.get(randomIndex);
    }

    Card firstCard;
    try {
      firstCard = discardPile.placeFirstCard(this);
    } catch (StackOverflowError e) {
      discardPile.clearPile();
      resetDrawPile();
      initiateGame();
      return;
    }

    JSONObject jCard = new JSONObject();
    jCard.put("color", firstCard.getColor());
    jCard.put("value", firstCard.getValue());

    JSONObject nextTurnCmd = new JSONObject();
    nextTurnCmd.put("command", Command.NEXT_TURN);
    nextTurnCmd.put("playedCard", jCard);
    nextTurnCmd.put("displayName", currentPlayer.getName());
    nextTurnCmd.put("prevPlayerCardCount", 7);
    broadcast(nextTurnCmd);

    handleComputerPlayer();
  }

  public void shutdown() {
    Thread.currentThread().interrupt();
  }

  public void broadcast(JSONObject jo) {
    for (Player p : players) {
      if (p instanceof HumanPlayer) {
        ((HumanPlayer) p).getHandler().sendObject(jo);
      }
    }
  }
}
