package nl.utwente.cardgame.Controller;

import nl.utwente.cardgame.Controller.Client.Client;
import nl.utwente.cardgame.Controller.Server.Server;

public class Main {
  public static void main(String[] args) {
    if(args[0].equals("server")) {
      int port = Integer.parseInt(args[1]);
      Thread server = new Thread(new Server(port));
      server.start();
    } else {
      Client client = new Client();
      client.run();
    }
  }
}