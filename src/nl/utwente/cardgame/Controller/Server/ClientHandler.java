package nl.utwente.cardgame.Controller.Server;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.NetworkProtocol.Command;
import nl.utwente.cardgame.Model.NetworkProtocol.Error;
import nl.utwente.cardgame.Model.NetworkProtocol.Functionality;
import nl.utwente.cardgame.Model.Player.HumanPlayer;
import nl.utwente.cardgame.Model.Player.Player;
import nl.utwente.cardgame.Model.Utils.JSON;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ClientHandler implements Runnable {

  private Socket socket;
  private Server server;
  private BufferedReader in;
  private PrintWriter out;
  private String displayName;
  private final String PROTOCOL_VERSION = "1.2";
  private ArrayList<Functionality> clientFunctionalities;
  private ArrayList<ClientHandler> queue;
  private Game game;

  public ClientHandler(Socket socket, Server server) {
    this.socket = socket;
    this.server = server;
    clientFunctionalities = new ArrayList<>();
  }

  @Override
  public void run() {
    try {
      out = new PrintWriter(socket.getOutputStream(), true);
      in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      String req;

      while ((req = in.readLine()) != null) {
        try {
          JSONObject data = new JSONObject(req);
          Command cmd = data.getEnum(Command.class, "command");

          if (displayName == null) {
            handleInitialHandshake(cmd, data);
          } else {
            switch (cmd) {
              case PLAY_CARD -> {
                Card card = JSON.getInstance().objectToCard(data.getJSONObject("card"));
                Player player = game.getPlayer(displayName);
                game.playCard(card, player);
              }
              case DRAW_CARD -> {
                Player player = game.getPlayer(displayName);
                game.drawCard(player);
              }
              case CHAT -> {
                data.put("displayName", displayName);
                if (queue == null && game == null){
                  server.broadcast(server.getLobby(), data);
                } else if (queue != null) {
                  server.broadcast(queue, data);
                } else if (game != null) {
                  game.broadcast(data);
                }
              }
              case JOIN_GAME -> joinGame(data);
            }
          }
        } catch (JSONException e) {
          JSONObject jo = new JSONObject()
                  .put("command", Command.ERROR)
                  .put("code", Error.INVALID_COMMAND)
                  .put("description", "Could not parse the client's message to JSON. Is it formatted correctly?");
          out.println(jo);
        }
      }
    } catch (IOException e) {
      if (e.getMessage().equals("Connection reset")) {
        shutdown();
      }
    }
  }

  private void handleInitialHandshake(Command cmd, JSONObject data) {
    String clientProtocolVersion = data.getString("protocolVersion");
    JSONObject jo = new JSONObject();

    if (cmd == Command.HI) {
      if(!clientProtocolVersion.equals(PROTOCOL_VERSION)) {
        jo.put("command", Command.ERROR);
        jo.put("code", Error.INCOMPATIBLE_VERSION);
        jo.put("description", "The version that the client is trying to use (" + clientProtocolVersion
                + ") is not compatible with the protocol version of the server (" + PROTOCOL_VERSION + ").");
        out.println(jo);
        shutdown();
      } else {
        JSONArray receivedFunctionalities = data.getJSONArray("functionalities");
        if (receivedFunctionalities != null) {
          for (int i = 0; i < receivedFunctionalities.length(); i++) {
            clientFunctionalities.add(receivedFunctionalities.optEnum(Functionality.class, i));
          }
        } else {
          jo.put("command", Command.ERROR);
          jo.put("code", Error.INVALID_ARGUMENT);
          jo.put("description", "The client did not specify the supported functionalities in an array of 'Functionality'.");
        }

        String inputDisplayName = data.getString("displayName");
        boolean validName = true;

        for (ClientHandler client : server.getConnections()) {
          if (client.getDisplayName() != null && client.getDisplayName().equals(inputDisplayName)) {
            jo.put("command", Command.ERROR);
            jo.put("code", Error.INVALID_NAME);
            jo.put("description", "This nickname is already taken. Please choose a different nickname.");
            out.println(jo);
            validName = false;
          }
        }

        if (validName) {
          displayName = data.getString("displayName");
          jo.put("command", Command.CONNECTED);
          out.println(jo);
          server.addToLobby(this);
        }
      }
    } else {
      jo.put("command", Command.ERROR)
              .put("code", Error.INVALID_COMMAND)
              .put("description", "Please present yourself to the server with the HI command with a displayName argument first.");
    }
  }

  public void joinGame(JSONObject data) {
    int gameSize = data.getInt("gameSize");
    server.joinQueue(this, gameSize);
    broadcastQueueJoined(gameSize);
    server.getLobby().remove(this);
    boolean onlyComputerPlayers = data.has("onlyComputerPlayers") && data.getBoolean("onlyComputerPlayers");

    if (queue.size() == 1 && !onlyComputerPlayers) {
      startTimeLeftBroadcaster(gameSize);
    }

    if (queue.size() == gameSize || onlyComputerPlayers) {
      server.newGame(gameSize, onlyComputerPlayers);
    }
  }

  private void broadcastQueueJoined(int gameSize) {
    JSONObject queueJoinedCommand = new JSONObject();
    queueJoinedCommand.put("command", Command.QUEUE_JOINED);
    queueJoinedCommand.put("displayName", displayName);

    ArrayList<String> playerNames = new ArrayList<>();
    queue = server.getQueues().get(gameSize);

    for (ClientHandler ch : queue) {
      playerNames.add(ch.getDisplayName());
    }

    queueJoinedCommand.put("playerNames", playerNames);

    for (ClientHandler ch : queue) {
      ch.sendObject(queueJoinedCommand);
    }
  }

  private void startTimeLeftBroadcaster(int gameSize) {
    Timer timer = new Timer();
    final int TIMER_SECONDS = 30;
    final int[] secondsLeft = {TIMER_SECONDS};

    // Start timer in separate thread.
    timer.scheduleAtFixedRate(new TimerTask() {
      @Override
      public void run() {
        if (secondsLeft[0] == 5) {

          final int PERIOD = 1;
          timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
              if (secondsLeft[0] == 0) {
                if(queue != null && queue.size() > 0) server.newGame(gameSize, false);
                this.cancel();
              } else {
                JSONObject jo = new JSONObject();
                jo.put("command", Command.CHAT);
                jo.put("displayName", "server");
                jo.put("message", "The game will start in " + secondsLeft[0] + "...");
                server.broadcast(queue, jo);
                secondsLeft[0] -= PERIOD;
              }
            }
          }, 0, PERIOD*1000);

        } else if (secondsLeft[0] > 5) {
          JSONObject jo = new JSONObject();
          jo.put("command", Command.CHAT);
          jo.put("displayName", "server");
          jo.put("message", "The game will start in " + secondsLeft[0] + " seconds.");
          server.broadcast(queue, jo);
          secondsLeft[0] -= 5;
        }
      }
    }, 0, 5 *1000L);
  }

  public synchronized void sendChat(String msg) {
    if (clientFunctionalities.contains(Functionality.CHAT)) {
      out.println(msg);
      JSONObject jo = new JSONObject();
      jo.put("command", Command.CHAT);
      jo.put("message", msg);
      jo.put("displayName", displayName);
      server.broadcast(jo.toString());
    }
  }

  public synchronized void sendObject(JSONObject jo) {
    out.println(jo);
  }

  public void shutdown() {
    try {
      System.out.println(displayName + " disconnected.");
      in.close();
      out.close();

      if (queue != null && game == null) {
        queue.remove(this);
        JSONObject jo = new JSONObject();
        jo.put("command", Command.QUEUE_LEFT);

        ArrayList<String> playerNames = new ArrayList<>();
        for (ClientHandler client : queue){
          playerNames.add(client.getDisplayName());
        }
        jo.put("playerNames", playerNames);
        jo.put("displayName", displayName);
        server.broadcast(queue, jo);
      }

      server.removeConnection(this);

      if (game != null) {
        JSONObject jo = new JSONObject();
        jo.put("command", Command.ERROR);
        jo.put("code", Error.PLAYER_DISCONNECTED);
        jo.put("description", "A player disconnected and the game has been stopped.");
        game.broadcast(jo);
        server.getGames().remove(game);
        game.shutdown();

        for (Player p : game.getPlayers()) {
          if (p instanceof HumanPlayer) {
            ((HumanPlayer) p).getHandler().setGame(null);
          }
        }
      }

      Thread.currentThread().interrupt();
    } catch (IOException e) {
      shutdown();
    }
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public ArrayList<Functionality> getFunctionalities() {
    return clientFunctionalities;
  }

  public void setGame(Game game) {
    this.game = game;
  }

  public void setQueue(ArrayList<ClientHandler> queue) {
    this.queue = queue;
  }

}