package nl.utwente.cardgame.Controller.Server;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.NetworkProtocol.Command;
import nl.utwente.cardgame.Model.NetworkProtocol.Functionality;
import nl.utwente.cardgame.Model.Player.ComputerPlayer;
import nl.utwente.cardgame.Model.Player.HumanPlayer;
import nl.utwente.cardgame.Model.Player.Player;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {

  private ArrayList<ClientHandler> connections;
  private ServerSocket server;
  private boolean openForConnections;
  private ExecutorService clientPool;
  private ExecutorService gamesPool;
  private int port;
  private HashMap<Integer, ArrayList<ClientHandler>> queues;
  private ArrayList<Game> games;
  private ArrayList<ClientHandler> lobby;
  protected final Functionality[] SERVER_FUNCTIONALITIES;

  public Server(int port) {
    this.port = port;
    connections = new ArrayList<>();
    queues = new HashMap<>();
    openForConnections = true;
    SERVER_FUNCTIONALITIES = new Functionality[]{Functionality.CHAT, Functionality.LOBBY, Functionality.MULTI_GAME};
    games = new ArrayList<>();
    lobby = new ArrayList<>();
  }

  /**
   * Adds a game to the thread pool of all games, and starts that game.
   * @param gameSize - The number of players that will be in this game.
   */
  public void newGame(int gameSize, boolean onlyComputerPlayers) {
    ArrayList<Functionality> functionalities = new ArrayList<>(List.of(SERVER_FUNCTIONALITIES));

    Game game = new Game(7, onlyComputerPlayers);

    for(ClientHandler handler : queues.get(gameSize)) {
      Player p = new HumanPlayer(handler);
      try {
        game.addPlayer(p);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    ArrayList<String> playerNames = new ArrayList<>();
    for (ClientHandler ch : queues.get(gameSize)) {
      playerNames.add(ch.getDisplayName());
    }

    if (gameSize > queues.get(gameSize).size() || onlyComputerPlayers) {
      for (int i = (onlyComputerPlayers ? 0 : queues.get(gameSize).size()); i < gameSize; i++) {
        Player p = new ComputerPlayer("Computer player " + (i - queues.get(gameSize).size() + 1));

        playerNames.add(p.getName());
        JSONObject computerPlayerJoined = new JSONObject();
        computerPlayerJoined.put("command", Command.QUEUE_JOINED);
        computerPlayerJoined.put("displayName", p.getName());
        computerPlayerJoined.put("playerNames", playerNames);
        broadcast(queues.get(gameSize), computerPlayerJoined);

        try {
          game.addPlayer(p);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }

    for(ClientHandler handler : queues.get(gameSize)) {
      handler.setGame(game);
    }

    for (ClientHandler handler : queues.get(gameSize)) {
      handler.setQueue(null);
    }

    for (ClientHandler handler : queues.get(gameSize)) {
      for (Functionality functionality : functionalities) {
        if (!handler.getFunctionalities().contains(functionality)) {
          functionalities.remove(functionality);
        }
      }
    }

    JSONObject gameStarted = new JSONObject();
    gameStarted.put("command", Command.GAME_STARTED);
    gameStarted.put("functionalities", functionalities);
    broadcast(queues.get(gameSize), gameStarted);

    games.add(game);
    gamesPool.execute(game);
    queues.remove(gameSize);
  }

  public HashMap<Integer, ArrayList<ClientHandler>> getQueues() {
    return queues;
  }

  public void joinQueue(ClientHandler client, int gameSize) {
    queues.computeIfAbsent(gameSize, k -> new ArrayList<>()).add(client);
  }

  @Override
  public void run() {
    try {
      server = new ServerSocket(port);
      clientPool = Executors.newCachedThreadPool();
      gamesPool = Executors.newCachedThreadPool();
      System.out.println("Server is listening for connections on port " + port + ".");

      while (openForConnections) {
        Socket client = server.accept();
        ClientHandler clientHandler = new ClientHandler(client, this);
        connections.add(clientHandler);
        clientPool.execute(clientHandler);
        System.out.println("New connection.");
      }
    } catch (IOException e) {
      System.out.println("Shutting down because of IOException");
      shutdown();
    }
  }

  public void broadcast(String msg) {
    for (ClientHandler ch : connections) {
      if (ch != null) {
        ch.sendChat(msg);
      }
    }
  }

  public void broadcast(ArrayList<ClientHandler> handlers, JSONObject jsonObject) {
    if (handlers != null) {
      for (ClientHandler ch : handlers) {
        if (ch != null) {
          ch.sendObject(jsonObject);
        }
      }
    }
  }

  public void shutdown() {
    System.out.println("Shutting down server socket");
    try {
      openForConnections = false;
      clientPool.shutdown();
      if(!server.isClosed()) {
        server.close();
      }

      for(ClientHandler handler : connections) {
        handler.shutdown();
      }
    } catch (IOException e) {
      shutdown();
    }
  }

  public ArrayList<ClientHandler> getConnections() {
    return connections;
  }

  public void removeConnection (ClientHandler clientHandler){
    connections.remove(clientHandler);
  }

  public ArrayList<Game> getGames() {
    return games;
  }

  public ArrayList<ClientHandler> getLobby() {
    return lobby;
  }

  public void addToLobby(ClientHandler client) {
    lobby.add(client);
  }
}
