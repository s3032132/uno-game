package nl.utwente.cardgame.Model.Card;

import nl.utwente.cardgame.Model.Card.CardActions.*;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.NetworkProtocol.CardValue;
import nl.utwente.cardgame.Model.Utils.ANSI;

public class ActionCard extends Card {
  private Action action;

  /**
   * Constructs a Card object for cards of type ACTION.
   * @ensures actionPerformed == false
   * @param color The color of the card.
   * @param action The action that will be performed when the card is played.
   */
  public ActionCard(CardColor color, Action action) {
    super(color);
    this.action = action;
  }

  /**
   * Returns the color of the Card.
   * @ensures CardColor != null
   * @return color of Card
   */

  public Action getAction() {
    return action;
  }

  @Override
  public String toString() {
    return "color " + ANSI.BLUE + getColor().toString() + ANSI.RESET + " and action " + ANSI.YELLOW
            + action.toString() + ANSI.RESET;
  }

  @Override
  public int getPoints (){
    Action action = this.getAction();
    if(action instanceof DrawAction || action instanceof ReverseAction || action instanceof SkipAction) {
      return 20;
    } else if(action instanceof WildAction || action instanceof WildDrawAction) {
      return 50;
    }
    return 0;
  }

  public CardValue getValue (){
    if(action instanceof DrawAction) {
      return CardValue.D;
    } else if (action instanceof WildAction) {
      return CardValue.W;
    } else if (action instanceof WildDrawAction) {
      return CardValue.F;
    } else if (action instanceof ReverseAction) {
      return CardValue.R;
    } else if (action instanceof SkipAction) {
      return CardValue.S;
    }
    return null;
  }
}
