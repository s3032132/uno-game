package nl.utwente.cardgame.Model.Card;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.Card.CardActions.WildAction;
import nl.utwente.cardgame.Model.Card.CardActions.WildDrawAction;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.NetworkProtocol.CardValue;

import java.util.Objects;

public abstract class Card {
    private CardColor color;

    /**
     * Constructs a new Card object and sets the color of it.
     * @param color
     */
    public Card(CardColor color) {
        this.color = color;
    }

    /**
     * Returns the color of the card
     * @return the card color of this <code>Card</>
     */
    public CardColor getColor() {
        return color;
    }

    public void setColor(CardColor color) {
        this.color = color;
    }

    /**
     * Returns the value of each card left at the end of the game.
     * @return number of points
     */
    public abstract int getPoints ();

    @Override
    public abstract String toString();

    /**
     * Checks whether the card that the player wants to play is valid.
     * @return <code>true</code> if the move is valid, <code>false</code> if the move is invalid.
     */
    public boolean isValidMove(Game game) {

        Card topCard = game.getDiscardPile().getTopCard();
        if (color == game.getCurrentColor() || color == CardColor.W) {
            return true;
        }

        if(this instanceof NumberCard && topCard instanceof NumberCard) {
            if (((NumberCard) this).getNumber() == ((NumberCard) topCard).getNumber()) {
                return true;
            }
        }

        if (this instanceof ActionCard){
            if (topCard instanceof ActionCard) {
                // This is a horrible workaround, since card.getAction() == topCard.getAction() does not work for some reason.
                if (Objects.equals(((ActionCard) this).getAction().toString(), ((ActionCard) topCard).getAction().toString())) {
                    return true;
                }
            }

            if (((ActionCard) this).getAction() instanceof WildAction) {
                return true;
            }

            // Wild draw four card is only valid if you do not have a number card with the same color as the current color.
            if (((ActionCard) this).getAction() instanceof WildDrawAction) {
                boolean validWildDraw = true;
                for (Card testCard : game.getCurrentPlayer().getHand().getCards()) {
                    if(testCard instanceof NumberCard) {
                        if (testCard.getColor() == game.getCurrentColor()) {
                            validWildDraw = false;
                        }
                    }
                }
                return validWildDraw;
            }
        }

        return false;
    }


    public abstract CardValue getValue();

}
