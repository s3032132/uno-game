package nl.utwente.cardgame.Model.Card.CardActions;

import nl.utwente.cardgame.Controller.Game;

public interface Action {
  void performAction(Game game);
  @Override
  String toString();
}
