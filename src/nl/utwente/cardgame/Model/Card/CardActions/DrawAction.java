package nl.utwente.cardgame.Model.Card.CardActions;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.NetworkProtocol.Command;
import nl.utwente.cardgame.Model.Player.HumanPlayer;
import nl.utwente.cardgame.Model.Player.Player;
import org.json.JSONArray;
import org.json.JSONObject;

public class DrawAction implements Action {
  @Override
  public void performAction(Game game) {
    Player p = game.getCurrentColor() == null ? game.getCurrentPlayer() : game.getNextPlayer();

    JSONObject issueCardsCmd = new JSONObject();
    issueCardsCmd.put("command", Command.ISSUE_CARD);
    JSONArray cardsToIssue = new JSONArray();

    for (int i = 0; i < 2; i++) {
      Card card = p.drawCard(game.getDrawPile());
      JSONObject jCard = new JSONObject();
      jCard.put("color", card.getColor());
      jCard.put("value", card.getValue());
      cardsToIssue.put(jCard);
    }

    issueCardsCmd.put("card", cardsToIssue);

    if (p instanceof HumanPlayer) {
      ((HumanPlayer) p).getHandler().sendObject(issueCardsCmd);
    }

    game.nextPlayer();
  }

  @Override
  public String toString(){
    return "next player draws 2 cards";
  }
}
