package nl.utwente.cardgame.Model.Card.CardActions;

import nl.utwente.cardgame.Controller.Game;

public class ReverseAction implements Action {
  @Override
  public void performAction(Game game) {
    game.switchDirection();
  }

  @Override
  public String toString(){
    return "reverse direction";
  }
}
