package nl.utwente.cardgame.Model.Card.CardActions;

import nl.utwente.cardgame.Controller.Game;

public class SkipAction implements Action {
  @Override
  public void performAction(Game game) {
    game.nextPlayer();
  }

  @Override
  public String toString(){
    return "skip next player";
  }
}
