package nl.utwente.cardgame.Model.Card;

import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.NetworkProtocol.CardValue;
import nl.utwente.cardgame.Model.Utils.ANSI;

public class NumberCard extends Card {
  private int number;

  /**
   * Constructs a Card object for cards of type NUMBER.
   * @requires CardType == NUMBER
   * @param number The number on the card.
   * @param color The color of the card.
   */
  public NumberCard(CardColor color, int number) {
    super(color);
    this.number = number;
  }

  /**
   * Returns the number of the Card.
   * @requires CardType == NUMBER
   * @ensures 0 <= number <= 9
   * @return number of card if it is of CardType NUMBER, otherwise -1
   */
  public int getNumber() {
    return number;
  }

  @Override
  public String toString(){
    return "color " + ANSI.BLUE + getColor().toString() + ANSI.RESET + " and number " + ANSI.BLUE_BOLD + number
            + ANSI.RESET;
  }

  @Override
  public int getPoints(){
    return this.getNumber();
  }

  @Override
  public CardValue getValue() {
    switch (number) {
      case 0 -> { return CardValue.ZERO; }
      case 1 -> { return CardValue.ONE; }
      case 2 -> { return CardValue.TWO; }
      case 3 -> { return CardValue.THREE; }
      case 4 -> { return CardValue.FOUR; }
      case 5 -> { return CardValue.FIVE; }
      case 6 -> { return CardValue.SIX; }
      case 7 -> { return CardValue.SEVEN; }
      case 8 -> { return CardValue.EIGHT; }
      case 9 -> { return CardValue.NINE; }
    }

    return null;
  }
}
