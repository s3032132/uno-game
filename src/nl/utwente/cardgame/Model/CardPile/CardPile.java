package nl.utwente.cardgame.Model.CardPile;

import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.NetworkProtocol.CardValue;

import java.util.ArrayList;

public class CardPile {
  private ArrayList<Card> cards;

  /**
   * Creates a new Arraylist with cards
   * @param cards
   */
  public CardPile(ArrayList<Card> cards) {
    this.cards = cards;
  }

  /**
   * Adds a card to the current ArrayList.
   * @param card
   */
  public void addCard(Card card) {
    cards.add(card);
  }

  /**
   * Removes a card from the current ArrayList.
   * @param card
   */
  void removeCard(Card card) {
    ArrayList<Card> cardsCopy = new ArrayList<>(cards);
    for (Card loopCard : cardsCopy) {
      if (loopCard.getValue() == card.getValue() && loopCard.getColor() == card.getColor()) {
        cards.remove(loopCard);
        break;
      } else if (loopCard.getValue() == card.getValue()
              && (loopCard.getValue() == CardValue.W || loopCard.getValue() == CardValue.F)) {
        cards.remove(loopCard);
      }
    }
  }

  public void clearPile() {
    cards = new ArrayList<>();
  }

  /**
   * Get all cards in the CardPile. Only accessible to other instances of CardPile (package-private).
   * @return Cards of the deck
   */
  public ArrayList<Card> getCards() {
    return cards;
  }
}
