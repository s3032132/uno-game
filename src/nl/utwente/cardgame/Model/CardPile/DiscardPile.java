package nl.utwente.cardgame.Model.CardPile;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.Card.ActionCard;
import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.Card.CardActions.Action;
import nl.utwente.cardgame.Model.Card.CardActions.DrawAction;
import nl.utwente.cardgame.Model.Card.CardActions.WildAction;
import nl.utwente.cardgame.Model.Card.CardActions.WildDrawAction;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.NetworkProtocol.CardValue;

import java.util.ArrayList;

public class DiscardPile extends CardPile {

  /**
   * Constructs a new DiscardPile object
   * @ensures the ArrayList created is empty
   */
  public DiscardPile(DrawPile drawPile) {
    super(new ArrayList<>());
    addCard(drawPile.pickCard());
  }

  /**
   * Returns the last card that has been played over the DiscardPile
   * @return Card - last valid card played
   */
  public Card getTopCard() {
    return getCards().get(getCards().size() - 1);
  }

  /**
   * Removes all the cards from the discard pile and adds them to the deck.
   * @param drawPile - The deck to which all the cards have to be added.
   */
  public void passToDrawPile(DrawPile drawPile, Game game) {
    for (Card card : getCards()) {
      if (card.getValue() == CardValue.W || card.getValue() == CardValue.F) {
        card.setColor(CardColor.W);
      }
      drawPile.addCard(card);
    }
    clearPile();
    placeFirstCard(game);
  }


  /**
   * Places a first card on the discard pile (the top card). This method abides to all the rules of UNO.
   * @ensures top card is not a Wild Draw Four Card
   * @param game
   */
  public Card placeFirstCard(Game game) {
    DrawPile drawPile = game.getDrawPile();
    Card card = drawPile.pickCard();
    addCard(card);

    boolean validCard = false;
    while(!validCard) {
      if (card instanceof ActionCard) {
        Action action = ((ActionCard) card).getAction();

        if (action instanceof WildDrawAction || action instanceof WildAction) {
          passToDrawPile(drawPile, game);
          card = drawPile.pickCard();
          addCard(card);
        } else {
          action.performAction(game);

          if (action instanceof DrawAction) {
            game.nextPlayer();
          }

          validCard = true;
        }
      } else {
        game.setCurrentColor(card.getColor());
        validCard = true;
      }
    }

    return card;
  }
}
