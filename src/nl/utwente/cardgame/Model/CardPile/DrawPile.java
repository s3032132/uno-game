package nl.utwente.cardgame.Model.CardPile;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.Card.ActionCard;
import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.Card.CardActions.*;
import nl.utwente.cardgame.Model.Card.NumberCard;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;

import java.util.ArrayList;

public class DrawPile extends CardPile {
  private ArrayList<CardColor> colors;
  private Game game;

  /**
   * Constructs a Deck object.
   * @ensures the ArrayList with the cards to be empty.
   */
  public DrawPile(Game game) {
    super(new ArrayList<>());
    this.game = game;

    colors = new ArrayList<>();
    colors.add(CardColor.R);
    colors.add(CardColor.Y);
    colors.add(CardColor.G);
    colors.add(CardColor.B);

    initiateNumberCards();
    initiateColorActionCards();
    initiateWildActionCards();
  }

  /**
   * Adds all colored numbered cards to the deck.
   * There are two cards of each number, with numbers ranging from 0-9.
   */
  private void initiateNumberCards() {
    for(int i = 0; i <= 9; i++) {
      for(CardColor color : colors) {
        addCard(new NumberCard(color, i));
        if(i > 0) addCard(new NumberCard(color, i));
      }
    }
  }

  /**
   * Adds all possible colored action cards to the deck.
   * There are three types of colored action cards.
   */
  private void initiateColorActionCards() {
    for (int type = 0; type < 3; type++) {
      for (CardColor color : colors) {
        for (int i = 0; i < 2; i++) {
          Action action;
          if (type == 0) {
            action = new DrawAction();
          } else if (type == 1) {
            action = new ReverseAction();
          } else {
            action = new SkipAction();
          }
          addCard(new ActionCard(color, action));
        }
      }
    }
  }

  /**
   * Adds all possible wild action cards to the deck.
   */
  private void initiateWildActionCards() {
    for (int i = 0; i < 4; i++) {
      this.addCard(new ActionCard(CardColor.W, new WildAction()));
      this.addCard(new ActionCard(CardColor.W, new WildDrawAction()));
    }
  }

  /**
   * Gets a random card from the Deck.
   * @return A card from the deck.
   */
  public Card getRandomCard() {
    // (Math.random() * range) + min [but here min = 0]
    int randomIndex = (int) (Math.random() * getCards().size());
    return getCards().get(randomIndex);
  }

  /**
   * Picks a random card from the <code>Deck</code>. This <u>removes</u> it from the <code>Deck</code>.
   * @return A card from the deck.
   */
  public Card pickCard() {
    Card card = getRandomCard();
    removeCard(card);
    if(getCards().size() == 0) {
      game.getDiscardPile().passToDrawPile(game.getDrawPile(), game);
    }
    return card;
  }

}
