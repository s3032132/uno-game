package nl.utwente.cardgame.Model.CardPile;

import nl.utwente.cardgame.Model.Card.Card;

import java.util.ArrayList;

public class Hand extends CardPile {

    /**
     * Constructs a new Hand
     * @ensures ArrayList is empty in the beggining
     */
    public Hand() {
        super(new ArrayList<>());
    }

    /**
     * Removes the <code>Card</code> from the player's <code>Hand</code> and adds it to the <code>DiscardPile</code>.
     * @requires Card isValidMove
     * @param card
     */
    public void discardCard(Card card, DiscardPile discardPile) {
        removeCard(card);
        discardPile.addCard(card);
    }

    @Override
    public String toString() {
        String output = "You have the following cards in your hand:";
        for(int i = 0; i < getCards().size(); i++) {
            output += "\n" + i + ". " + getCards().get(i).toString();
        }
        return output;
    }
}
