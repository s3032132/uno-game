package nl.utwente.cardgame.Model.Exceptions;

public class MaxPlayersReachedException extends Exception {
  public MaxPlayersReachedException(String message) {
    super(message);
  }
}
