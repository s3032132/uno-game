package nl.utwente.cardgame.Model.Exceptions;

public class PlayerAlreadyInGameException extends Exception {
  public PlayerAlreadyInGameException(String message) {
    super(message);
  }
}
