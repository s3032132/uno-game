package nl.utwente.cardgame.Model.Exceptions;

public class PlayerDoesNotExistException extends Exception {
  public PlayerDoesNotExistException() {
    super();
  }
}
