package nl.utwente.cardgame.Model;

public enum GameDirection {
  CLOCKWISE, COUNTER_CLOCKWISE
}
