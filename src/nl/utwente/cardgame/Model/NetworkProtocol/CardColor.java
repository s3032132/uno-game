package nl.utwente.cardgame.Model.NetworkProtocol;

public enum CardColor {
  R, Y, G, B, W
}
