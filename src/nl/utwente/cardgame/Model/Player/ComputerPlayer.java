package nl.utwente.cardgame.Model.Player;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.Card.ActionCard;
import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.Card.CardActions.WildAction;
import nl.utwente.cardgame.Model.Card.CardActions.WildDrawAction;
import nl.utwente.cardgame.Model.Card.NumberCard;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;

public class ComputerPlayer extends Player {
  public ComputerPlayer(String name) {
    super(name);
  }

  /**
   * Selects the best card to play for a ComputerPlayer.
   * @param game - This game.
   * @return <code>NumberCard</code> or <code>ActionCard</code>, or <code>null</code> if there is no playable card.
   */
  public Card selectCardToPlay(Game game) {
    Card chosenCard = null;
    Card topCard = game.getDiscardPile().getTopCard();

    for (Card card : getHand().getCards()) {
      if (card.getColor() == game.getCurrentColor() && chosenCard == null) {
        chosenCard = card;
      }

      if (card instanceof ActionCard) {
        if (topCard instanceof ActionCard) {
          if (((ActionCard) topCard).getAction() == ((ActionCard) card).getAction()) {
            if (!(((ActionCard) card).getAction() instanceof WildAction)
                    || !(((ActionCard) card).getAction() instanceof WildDrawAction)) {
              chosenCard = card;
            }
          }
        }
      }

      if (card instanceof NumberCard) {
        if (topCard instanceof NumberCard) {
          if (((NumberCard) card).getNumber() == ((NumberCard) topCard).getNumber()) {
            chosenCard = card;
          }
        }
      }
    }

    // Test for wild cards
    for (Card card : getHand().getCards()) {
      if (chosenCard == null && card instanceof ActionCard) {
        if (((ActionCard) card).getAction() instanceof WildDrawAction) {
          chosenCard = card;
          CardColor chosenColor = calculateBestWildcardColor();
          card.setColor(chosenColor);
        } else if (((ActionCard) card).getAction() instanceof WildAction) {
          chosenCard = card;
          CardColor chosenColor = calculateBestWildcardColor();
          card.setColor(chosenColor);
        }
      }
    }

    return chosenCard;
  }

  public CardColor calculateBestWildcardColor() {
    CardColor bestColor = CardColor.R;

    int red = 0;
    int yellow = 0;
    int blue = 0;
    int green = 0;

    // The best color is the color with the most amount of cards.
    for (Card card: getHand().getCards()) {
      switch (card.getColor()) {
        case R -> red++;
        case Y -> yellow++;
        case B -> blue++;
        case G -> green++;
      }
    }

    int max = Math.max(Math.max(Math.max(red,yellow),blue),green);
    if (max == yellow) bestColor = CardColor.Y;
    if (max == green) bestColor = CardColor.G;
    if (max == blue) bestColor = CardColor.B;

    return bestColor;
  }
}
