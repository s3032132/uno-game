package nl.utwente.cardgame.Model.Player;

import nl.utwente.cardgame.Controller.Server.ClientHandler;

public class HumanPlayer extends Player {
  private ClientHandler handler;

  public HumanPlayer(ClientHandler handler) {
    super(handler.getDisplayName());
    this.handler = handler;
  }

  public ClientHandler getHandler() {
    return handler;
  }
}
