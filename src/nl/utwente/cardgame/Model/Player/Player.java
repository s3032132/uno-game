package nl.utwente.cardgame.Model.Player;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.Card.ActionCard;
import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.CardPile.DrawPile;
import nl.utwente.cardgame.Model.CardPile.Hand;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.NetworkProtocol.Command;
import org.json.JSONObject;

public abstract class Player {
  private String name;
  private Hand hand;
  private int points;

  public Player(String name){
    this.name = name;
    this.hand = new Hand();
  }

  /**
   * Draws a Card from the Deck and adds it to their Hand.
   * @param drawPile The deck to draw a card from.
   */
  public Card drawCard(DrawPile drawPile) {
    Card card = drawPile.pickCard();
    getHand().addCard(card);
    return card;
  };

  public Hand getHand() {
    return hand;
  }

  /**
   * Makes the player play a card in their hand.
   * @requires the card is in the player's deck.
   * @requires the play is valid.
   * @ensures the card is played.
   * @param game
   */
  public void playCard(Game game, Card card) {
    if (card instanceof ActionCard) {
      ((ActionCard) card).getAction().performAction(game);
    }
    hand.discardCard(card, game.getDiscardPile());

    if (card.getColor() != CardColor.W) game.setCurrentColor(card.getColor());
  }

  /**
   * @return The name of the player.
   */
  public String getName() {
    return name;
  }

  public int calculateTotalPoints (Game game){
    int addedPoints = 0;

    for (Player p : game.getPlayers()) {
      for (Card card : p.getHand().getCards()) {
        addedPoints += card.getPoints();
      }

      p.getHand().clearPile();
    }

    points += addedPoints;
    return addedPoints;
  }

  public void calculatePoints(Game game){
    calculateTotalPoints(game);

    JSONObject roundFinished = new JSONObject();
    roundFinished.put("command", Command.ROUND_FINISHED);
    roundFinished.put("displayName", name);
    roundFinished.put("winnerScore", points);
    game.broadcast(roundFinished);

    if(points >= 500) {
      game.finish();
    } else {
      game.getDiscardPile().clearPile();
      game.resetDrawPile();
      game.initiateGame();
    }
  }

  public int getPoints() {
    return points;
  }
}
