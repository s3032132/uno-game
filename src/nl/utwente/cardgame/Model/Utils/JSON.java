package nl.utwente.cardgame.Model.Utils;

import nl.utwente.cardgame.Model.Card.ActionCard;
import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.Card.CardActions.*;
import nl.utwente.cardgame.Model.Card.NumberCard;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.NetworkProtocol.CardValue;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSON {
  private static JSON instance = null;

  private JSON() { }

  public static JSON getInstance() {
    if (instance == null) instance = new JSON();
    return instance;
  }

  public ArrayList<Card> ArrayToCards(JSONObject jo, String key) {
    JSONArray cards = jo.getJSONArray(key);
    ArrayList<Card> cardsArr = new ArrayList<>();
    for (int i = 0; i < cards.length(); i++) {
      Card card = objectToCard(cards.getJSONObject(i));
      cardsArr.add(card);
    }
    return cardsArr;
  }

  public Card objectToCard(JSONObject jCard) {
    CardColor color = jCard.getEnum(CardColor.class, "color");
    CardValue value = jCard.getEnum(CardValue.class, "value");
    Card card;

    switch (value) {
      case S -> card = new ActionCard(color, new SkipAction());
      case D -> card = new ActionCard(color, new DrawAction());
      case W -> card = new ActionCard(color, new WildAction());
      case F -> card = new ActionCard(color, new WildDrawAction());
      case R -> card = new ActionCard(color, new ReverseAction());
      case ZERO -> card = new NumberCard(color, 0);
      case ONE -> card = new NumberCard(color, 1);
      case TWO -> card = new NumberCard(color, 2);
      case THREE -> card = new NumberCard(color, 3);
      case FOUR -> card = new NumberCard(color, 4);
      case FIVE -> card = new NumberCard(color, 5);
      case SIX -> card = new NumberCard(color, 6);
      case SEVEN -> card = new NumberCard(color, 7);
      case EIGHT -> card = new NumberCard(color, 8);
      case NINE -> card = new NumberCard(color, 9);
      default -> card = null; // This should only happen if there's a problem
    }

    return card;
  }
}
