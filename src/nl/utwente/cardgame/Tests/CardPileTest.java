package nl.utwente.cardgame.Tests;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.CardPile.DrawPile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CardPileTest {
  Game game;
  DrawPile drawPile;

  @BeforeEach
  public void setup(){
    game = new Game(7, false);
    drawPile = new DrawPile(game);
  }

  @Test
  public void initialDeckTest() {
    assertEquals(108,drawPile.getCards().size());
  }

  @Test
  public void pickCardTest(){
    drawPile.pickCard();
    assertEquals(107,drawPile.getCards().size());
  }
}
