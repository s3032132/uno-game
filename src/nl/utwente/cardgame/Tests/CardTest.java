package nl.utwente.cardgame.Tests;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Model.Card.ActionCard;
import nl.utwente.cardgame.Model.Card.Card;
import nl.utwente.cardgame.Model.Card.CardActions.DrawAction;
import nl.utwente.cardgame.Model.Card.CardActions.ReverseAction;
import nl.utwente.cardgame.Model.Card.CardActions.SkipAction;
import nl.utwente.cardgame.Model.Card.CardActions.WildAction;
import nl.utwente.cardgame.Model.Card.NumberCard;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CardTest {

    Game game;

    @Test
    void toStringTest() {
        ActionCard skip = new ActionCard(CardColor.R, new SkipAction());
        ActionCard draw = new ActionCard(CardColor.R, new DrawAction());
        ActionCard reverse = new ActionCard(CardColor.R, new ReverseAction());
        ActionCard wild = new ActionCard(CardColor.W, new WildAction());

        assertTrue(draw.getAction().toString().equals("next player draws 2 cards"));
        assertTrue(skip.getAction().toString().equals("skip next player"));
        assertTrue(reverse.getAction().toString().equals("reverse direction"));
        assertTrue(wild.getAction().toString().equals("wild"));
    }

    @Test
    void isValidMoveNumberCardTest() {
        game = new Game(7, false);
        game.setCurrentColor(game.getDiscardPile().getTopCard().getColor());
        if (game.getDiscardPile().getTopCard() instanceof NumberCard){
            NumberCard cardTestForColor = new NumberCard(game.getCurrentColor(), 9);
            NumberCard cardTestForValue = new NumberCard(CardColor.R, ((NumberCard) game.getDiscardPile().getTopCard()).getNumber());
            assertTrue(cardTestForColor.isValidMove(game));
            assertTrue(cardTestForValue.isValidMove(game));
        }
    }

    @Test
    void isValidMoveActionCardTest(){
        game = new Game(7, false);
        game.setCurrentColor(game.getDiscardPile().getTopCard().getColor());
        if (game.getDiscardPile().getTopCard() instanceof ActionCard){
            ActionCard cardTestForColor1 = new ActionCard(game.getCurrentColor(), new ReverseAction());
            ActionCard cardTestForColor2 = new ActionCard(game.getCurrentColor(), new SkipAction());
            ActionCard cardTestForColor3 = new ActionCard(game.getCurrentColor(), new DrawAction());;
            assertTrue(cardTestForColor1.isValidMove(game));
            assertTrue(cardTestForColor2.isValidMove(game));
            assertTrue(cardTestForColor3.isValidMove(game));
            switch (game.getDiscardPile().getTopCard().getValue()){
                case S -> {
                    ActionCard cardTestForAction1 = new ActionCard(CardColor.G, new SkipAction());
                    ActionCard cardTestForAction2 = new ActionCard(CardColor.R, new SkipAction());
                    ActionCard cardTestForAction3 = new ActionCard(CardColor.B, new SkipAction());
                    ActionCard cardTestForAction4 = new ActionCard(CardColor.Y, new SkipAction());
                    assertTrue(cardTestForAction1.isValidMove(game));
                    assertTrue(cardTestForAction2.isValidMove(game));
                    assertTrue(cardTestForAction3.isValidMove(game));
                    assertTrue(cardTestForAction4.isValidMove(game));
                }
                case R -> {
                    ActionCard cardTestForAction1 = new ActionCard(CardColor.G, new ReverseAction());
                    ActionCard cardTestForAction2 = new ActionCard(CardColor.R, new ReverseAction());
                    ActionCard cardTestForAction3 = new ActionCard(CardColor.B, new ReverseAction());
                    ActionCard cardTestForAction4 = new ActionCard(CardColor.Y, new ReverseAction());
                    assertTrue(cardTestForAction1.isValidMove(game));
                    assertTrue(cardTestForAction2.isValidMove(game));
                    assertTrue(cardTestForAction3.isValidMove(game));
                    assertTrue(cardTestForAction4.isValidMove(game));
                }
                case D -> {
                    ActionCard cardTestForAction1 = new ActionCard(CardColor.G, new DrawAction());
                    ActionCard cardTestForAction2 = new ActionCard(CardColor.R, new DrawAction());
                    ActionCard cardTestForAction3 = new ActionCard(CardColor.B, new DrawAction());
                    ActionCard cardTestForAction4 = new ActionCard(CardColor.B, new DrawAction());
                    assertTrue(cardTestForAction1.isValidMove(game));
                    assertTrue(cardTestForAction2.isValidMove(game));
                    assertTrue(cardTestForAction3.isValidMove(game));
                    assertTrue(cardTestForAction4.isValidMove(game));
                }
                case W -> {
                    ActionCard cardTestForAction1 = new ActionCard(CardColor.W, new WildAction());
                    assertTrue(cardTestForAction1.isValidMove(game));
                }
            }
        }
    }
    @Test
    void setColorTest(){
        Card card = new NumberCard(CardColor.B,6);
        card.setColor(CardColor.Y);
        assertTrue(card.getColor().equals(CardColor.Y));
    }

    @Test
    void getColorTest(){
        Card card = new NumberCard(CardColor.B,6);
        Card testCard = new NumberCard(CardColor.Y,6);
        card.setColor(testCard.getColor());
        assertTrue(card.getColor().equals(CardColor.Y));
    }

    @Test
    void getPointsTest (){
        Card card = new NumberCard(CardColor.B,6);
        assertEquals(6, card.getPoints());
    }

}