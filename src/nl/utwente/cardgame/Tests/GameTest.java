package nl.utwente.cardgame.Tests;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Controller.Server.ClientHandler;
import nl.utwente.cardgame.Model.Exceptions.MaxPlayersReachedException;
import nl.utwente.cardgame.Model.Exceptions.PlayerAlreadyInGameException;
import nl.utwente.cardgame.Model.Player.HumanPlayer;
import nl.utwente.cardgame.Model.Player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameTest {
    Game game;
    Player p1;
    Player p2;
    Player p3;
    Player p4;
    ArrayList<Player> players;

    @BeforeEach
    void setUp() {
        game = new Game(7, false);
        players = new ArrayList<>();

        ClientHandler ch1 = new ClientHandler(null, null);
        ClientHandler ch2 = new ClientHandler(null, null);
        ClientHandler ch3 = new ClientHandler(null, null);
        ClientHandler ch4 = new ClientHandler(null, null);

        ch1.setDisplayName("Player 1");
        ch2.setDisplayName("Player 2");
        ch2.setDisplayName("Player 3");
        ch2.setDisplayName("Player 4");

        p1 = new HumanPlayer(ch1);
        p2 = new HumanPlayer(ch2);
        p3 = new HumanPlayer(ch3);
        p4 = new HumanPlayer(ch4);

        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);

        try {
            game.addPlayer(p1);
            game.addPlayer(p2);
            game.addPlayer(p3);
            game.addPlayer(p4);
        } catch (PlayerAlreadyInGameException | MaxPlayersReachedException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * Tests <code>getNextPlayer()</code> and the <code>nextPlayer()</code> method, in combination with the
     * <code>switchDirection()</code> method for switching game directions.
     */
    @Test
    void getAndSetNextPlayer() {
        int randomIndex = (int) (Math.random() * players.size());
        Player currentPlayer = players.get(randomIndex);
        game.setCurrentPlayer(currentPlayer);
        assertEquals(players.get(randomIndex), game.getCurrentPlayer());
        game.nextPlayer();
        if (randomIndex + 1 == game.getPlayers().size()){
            randomIndex = 0;
        }
        else {
            randomIndex += 1;
        }
        assertEquals(players.get(randomIndex), game.getCurrentPlayer());
        game.switchDirection();
        game.nextPlayer();

        if (randomIndex - 1 == -1){
            randomIndex = 3;
        }
        else {
            randomIndex -= 1;
        }

        assertEquals(players.get(randomIndex), game.getCurrentPlayer());
    }

    @Test
    void drawCardTest() {
        p1.drawCard(game.getDrawPile());
        assertEquals( 1, p1.getHand().getCards().size());
    }


}