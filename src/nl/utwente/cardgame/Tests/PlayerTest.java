package nl.utwente.cardgame.Tests;

import nl.utwente.cardgame.Controller.Game;
import nl.utwente.cardgame.Controller.Server.ClientHandler;
import nl.utwente.cardgame.Model.Card.ActionCard;
import nl.utwente.cardgame.Model.Card.CardActions.ReverseAction;
import nl.utwente.cardgame.Model.Card.CardActions.WildDrawAction;
import nl.utwente.cardgame.Model.Card.NumberCard;
import nl.utwente.cardgame.Model.CardPile.DrawPile;
import nl.utwente.cardgame.Model.Exceptions.MaxPlayersReachedException;
import nl.utwente.cardgame.Model.Exceptions.PlayerAlreadyInGameException;
import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.Player.ComputerPlayer;
import nl.utwente.cardgame.Model.Player.HumanPlayer;
import nl.utwente.cardgame.Model.Player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlayerTest {

    Game game;
    Player p1;
    Player p2;
    Player p3;
    Player p4;
    ArrayList<Player> players;
    @BeforeEach
    void setUp(){
        game = new Game(7, false);
        players = new ArrayList<>();

        ClientHandler ch1 = new ClientHandler(null, null);
        ClientHandler ch2 = new ClientHandler(null, null);
        ClientHandler ch3 = new ClientHandler(null, null);
        ClientHandler ch4 = new ClientHandler(null, null);

        ch1.setDisplayName("Player 1");
        ch2.setDisplayName("Player 2");
        ch2.setDisplayName("Player 3");
        ch2.setDisplayName("Player 4");

        p1 = new HumanPlayer(ch1);
        p2 = new HumanPlayer(ch2);
        p3 = new HumanPlayer(ch3);
        p4 = new HumanPlayer(ch4);

        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);

        try {
            game.addPlayer(p1);
            game.addPlayer(p2);
            game.addPlayer(p3);
            game.addPlayer(p4);
        } catch (PlayerAlreadyInGameException | MaxPlayersReachedException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void playCardTest() {
        ClientHandler ch1 = new ClientHandler(null, null);
        HumanPlayer player = new HumanPlayer(ch1);

        try {
            game.addPlayer(player);
        } catch (PlayerAlreadyInGameException e) {
            throw new RuntimeException(e);
        } catch (MaxPlayersReachedException e) {
            throw new RuntimeException(e);
        }

        NumberCard card1 = new NumberCard(CardColor.G, 8);
        player.getHand().addCard(card1);
        player.playCard(game, card1);
        assertEquals( 0,player.getHand().getCards().size());
    }

    @Test
    void calculateBestWildcardColorTest(){
        ComputerPlayer computerPlayer = new ComputerPlayer("PLayer 1");
        ActionCard card1 = new ActionCard(CardColor.Y, new ReverseAction());
        ActionCard card2 = new ActionCard(CardColor.W, new WildDrawAction());
        NumberCard card3 = new NumberCard(CardColor.G, 8);
        NumberCard card4 = new NumberCard(CardColor.G, 8);
        NumberCard card5 = new NumberCard(CardColor.B, 1);
        computerPlayer.getHand().addCard(card1);
        computerPlayer.getHand().addCard(card2);
        computerPlayer.getHand().addCard(card3);
        computerPlayer.getHand().addCard(card4);
        computerPlayer.getHand().addCard(card4);
        computerPlayer.getHand().addCard(card5);
        CardColor chosenColor = computerPlayer.calculateBestWildcardColor();
        assertEquals(CardColor.G, chosenColor);
    }

    @Test
    void getPointsTest() {
        game = new Game(3, false);
        players = new ArrayList<>();

        ClientHandler ch1 = new ClientHandler(null, null);
        ClientHandler ch2 = new ClientHandler(null, null);

        ch1.setDisplayName("Player 1");
        ch2.setDisplayName("Player 2");

        // P1 here is the winner of the round
        p1 = new HumanPlayer(ch1);
        p2 = new HumanPlayer(ch2);

        players.add(p1);
        players.add(p2);

        ActionCard card1 = new ActionCard(CardColor.Y, new ReverseAction());
        ActionCard card2 = new ActionCard(CardColor.W, new WildDrawAction());
        NumberCard card3 = new NumberCard(CardColor.G, 8);

        p2.getHand().addCard(card1);
        p2.getHand().addCard(card2);
        p2.getHand().addCard(card3);

        try {
            game.addPlayer(p1);
            game.addPlayer(p2);
        } catch (PlayerAlreadyInGameException | MaxPlayersReachedException e) {
            throw new RuntimeException(e);
        }
        p1.calculateTotalPoints(game);
        assertEquals(78, p1.getPoints());
    }
}