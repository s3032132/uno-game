package nl.utwente.cardgame.View;

import nl.utwente.cardgame.Model.NetworkProtocol.CardColor;
import nl.utwente.cardgame.Model.NetworkProtocol.CardValue;
import nl.utwente.cardgame.Model.NetworkProtocol.Command;
import nl.utwente.cardgame.Model.NetworkProtocol.Error;
import nl.utwente.cardgame.Model.Utils.ANSI;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class InputHandler {
  private static InputHandler instance = null;

  private InputHandler() { }

  public static InputHandler getInstance() {
    if (instance == null) instance = new InputHandler();
    return instance;
  }

  public void print(JSONObject obj) {
    Command cmd = obj.getEnum(Command.class, "command");

    switch (cmd) {
      case CONNECTED -> {
        printSuccess("Connection with the server has been established.");
        printLine("Please join a queue for a game by typing '/join <gameSize>' (gameSize = number of players)");
      }
      case QUEUE_JOINED -> {
        String newPlayerName = obj.getString("displayName");
        int playerCount = obj.getInt("playerCount");
        int gameSize = obj.getInt("gameSize");
        printSuccess(newPlayerName + " joined the queue. (" + playerCount + "/" + gameSize + ")");
      }
      case QUEUE_LEFT -> {
        String playerLeftName = obj.getString("displayName");
        printError(playerLeftName + " has left the queue.");
      }
      case GAME_STARTED -> {
        clear();
        printSuccess("\nThe game has started.");
      }
      case ROUND_STARTED -> {
        JSONArray cards = obj.getJSONArray("initialCards");
        printSuccess("A new round has started.\n");
        printLine("Your cards are:");
        printJSONCardsArray(cards);
      }
      case PLAY_CARD -> {
        JSONObject card = obj.getJSONObject("card");
        CardColor color = card.getEnum(CardColor.class, "color");
        CardValue value = card.getEnum(CardValue.class, "value");
        printLine("You played a " + color + " " + value + " card.");
      }
      case NEXT_TURN -> {
        String prevPlayerName = obj.getString("prevPlayerName");
        String displayName = obj.getString("thisPlayerName");
        String nextPlayerName = obj.has("displayName") ? obj.getString("displayName") : null;
        if (obj.has("playedCard")) {
          JSONObject card = obj.getJSONObject("playedCard");
          CardColor color = card.getEnum(CardColor.class, "color");
          CardValue value = card.getEnum(CardValue.class, "value");
          if (prevPlayerName.equals("")) {
            printLine("\nThe first card on the discard pile is a " + colorPrettier(color) + " " + valuePrettier(value) + ".\n");
          } else {
            printLine(prevPlayerName + " played card " + colorPrettier(color) + " " + valuePrettier(value) + "."
                    + " This is now the top card on the discard pile!\n");
          }
          if (value == CardValue.R) {
            printLine(ANSI.CYAN + "The game direction has been switched!\n");
          }
        } else {
          printLine(prevPlayerName + " drew a card.\n");
        }
        if(nextPlayerName != null && nextPlayerName.equals(displayName)) {
          printSuccess("It's your turn! Draw or play one of your cards:");
        } else if(nextPlayerName != null) {
          printLine("It's now " + ANSI.CYAN + nextPlayerName + ANSI.RESET + "'s turn.");
        }
        if (nextPlayerName != null && nextPlayerName.equals(displayName) && !prevPlayerName.equals("")) {
          printJSONCardsArray(obj.getJSONArray("cardsInHand"));
        }
      }
      case DRAW_CARD -> printLine("Drawing a card...");
      case ISSUE_CARD -> {
        printLine("You received the following card(s):");
        printJSONCardsArray(obj.getJSONArray("card"));
      }
      case ROUND_FINISHED -> {
        clear();
        String winnerName = obj.getString("displayName");
        int winnerScore = obj.getInt("winnerScore");
        printLine(ANSI.YELLOW_BACKGROUND + ANSI.BLACK_BOLD + winnerName + " has no cards left, and thus won this round.");
        printLine(ANSI.YELLOW_BACKGROUND + ANSI.BLACK_BOLD + "This brings them to a total of " + winnerScore
                + " points." + ANSI.RESET + "\n");
      }
      case GAME_FINISHED -> {
        JSONObject scoreboard = obj.getJSONObject("scoreboard");
        Map<String, Object> board = scoreboard.toMap();
        printLine(ANSI.YELLOW_BOLD + "------- SCOREBOARD -------");
        board.forEach((displayName, score) -> printLine(score + " - " + displayName));
        board.forEach((displayName, score) -> {
          if ((int) score >= 500) {
            printLine(ANSI.YELLOW_BOLD + "\n\uD83C\uDFC6 PLAYER " + displayName.toUpperCase()
                    + " HAS WON THE GAME! \uD83C\uDFC6\n");
          }
        });
        printLine("The game has ended. You can join a new game now using '/join <gameSize>'.");
      }
      case ERROR -> {
        printError(obj);
      }
      case CHAT -> {
        String message = obj.getString("message");
        String displayName = obj.getString("displayName");
        printLine("<" + ANSI.CYAN + displayName + ANSI.RESET + "> " + message);
      }
    }
  }

  public void printLine(String msg) {
    System.out.println(msg + ANSI.RESET);
  }

  private void printSuccess(String msg) {
    printLine(ANSI.GREEN_BRIGHT + msg);
  }

  private void printError(JSONObject obj) {
    Error code = obj.getEnum(Error.class, "code");
    String description = obj.getString("description");
    printError(code + ": " + description);
  }

  public void printError(String msg) {
    printLine(ANSI.RED_BRIGHT + msg + ANSI.RESET);
  }

  public void printJSONCardsArray(JSONArray cards) {
    for (int i = 0; i < cards.length(); i++){
      CardColor color = cards.getJSONObject(i).getEnum(CardColor.class, "color");
      CardValue value = cards.getJSONObject(i).getEnum(CardValue.class, "value");

      printLine(i + ". Color " + colorPrettier(color) + " and value " + valuePrettier(value));
    }
  }

  public synchronized String readLine() {
    try {
      InputStreamReader inputStreamReader = new InputStreamReader(System.in);
      BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
      return bufferedReader.readLine();
    } catch (IOException e) {
      printError("Please enter a valid command.");
    }

    return null;
  }

  public String getDisplayName() {
    printWelcome();
    printLine("Connecting status:");
    printLine("✅ entered hostname");
    printLine("✅ entered port number");
    printLine("➡️ entered nickname\n");
    printLine("Please enter a nickname:");
    return readLine();
  }

  public String getWildcardColor() {
    printLine("Please pick a color for your wildcard (red, yellow, green, blue):");
    return readLine();
  }

  public void printWelcome() {
    printLine(ANSI.BLUE_BRIGHT +
            ".------..------..------.\n" +
            "|U.--. ||N.--. ||O.--. |\n" +
            "| (\\/) || :(): || :/\\: |\n" +
            "| :\\/: || ()() || :\\/: |\n" +
            "| '--'U|| '--'N|| '--'O|\n" +
            "`------'`------'`------'\n");
    printLine(ANSI.BLUE_BOLD + "By Wouter Deen and Iulia Costea\n");
  }

  public String getHost() {
    printWelcome();
    printLine("Connecting to a server:");
    printLine("➡️ entered hostname");
    printLine("❌ entered port number");
    printLine("❌ entered nickname\n");
    printLine("Please enter the " + ANSI.GREEN_BOLD + "IP address" + ANSI.RESET + " (hostname) of the server:");
    while (true) {
      String input = readLine();
      int length = input.split("[.]").length;
      if (length == 4 || input.equals("localhost")) {
        clear();
        return input;
      } else {
        printError("Invalid hostname.");
      }
    }
  }

  public int getPort() {
    printWelcome();
    printLine("Connecting status:");
    printLine("✅ entered hostname");
    printLine("➡️ entered port number");
    printLine("❌ entered nickname\n");
    printLine("Please enter the " + ANSI.GREEN_BOLD  + "port number" + ANSI.RESET + " of the server:");
    int port = 0;
    while (port == 0) {
      try {
        int input = Integer.parseInt(readLine());
        if (input < 1 || input > 65535) {
          throw new NumberFormatException();
        } else port = input;
      } catch (NumberFormatException e) {
        printError("Invalid port number. Please enter a correct port number:");
      }
    }
    clear();
    return port;
  }

  public void printHelp() {
    printLine("---------------------------------------- COMMANDS HELP -----------------------------------------");
    printLine("/join <gameSize>            Join a game of a certain size.");
    printLine("/play <cardNumber>          Play the card of index <cardNumber>.");
    printLine("/direction                  View the direction of the game (clockwise or counter clockwise).");
    printLine("/draw                       Draw a card from the deck.");
    printLine("/topcard                    View the last card that was played.");
    printLine("/hand or /cards             View the cards that are in your hand.");
    printLine("/computergame <gameSize>    Starts a game with only computer players.");
    printLine("/help                       View this menu.");
  }

  public String colorPrettier(CardColor color) {
    return switch (color) {
      case R -> ANSI.RED_BRIGHT + "red";
      case Y -> ANSI.YELLOW_BRIGHT + "yellow";
      case G -> ANSI.GREEN_BRIGHT + "green";
      case B -> ANSI.BLUE + "blue";
      case W -> ANSI.RED_BRIGHT + "w" + ANSI.YELLOW_BRIGHT + "i" + ANSI.GREEN_BRIGHT + "l" + ANSI.BLUE_BRIGHT + "d";
    } + ANSI.RESET;
  }

  public String valuePrettier(CardValue value) {
    return switch (value) {
      case ZERO -> ANSI.PURPLE_BOLD_BRIGHT + "0";
      case ONE -> ANSI.PURPLE_BOLD_BRIGHT + "1";
      case TWO -> ANSI.PURPLE_BOLD_BRIGHT + "2";
      case THREE -> ANSI.PURPLE_BOLD_BRIGHT + "3";
      case FOUR -> ANSI.PURPLE_BOLD_BRIGHT + "4";
      case FIVE -> ANSI.PURPLE_BOLD_BRIGHT + "5";
      case SIX -> ANSI.PURPLE_BOLD_BRIGHT + "6";
      case SEVEN -> ANSI.PURPLE_BOLD_BRIGHT + "7";
      case EIGHT -> ANSI.PURPLE_BOLD_BRIGHT + "8";
      case NINE -> ANSI.PURPLE_BOLD_BRIGHT + "9";
      case S -> ANSI.PURPLE_BOLD + "skip next player";
      case D -> ANSI.PURPLE_BOLD + "draw 2 cards (skips turn)";
      case W -> ANSI.RED_BRIGHT + "w" + ANSI.YELLOW_BRIGHT + "i" + ANSI.GREEN_BRIGHT + "l" + ANSI.BLUE_BRIGHT + "d";
      case F -> ANSI.YELLOW_BOLD_BRIGHT + "w" + ANSI.RESET + ANSI.RED_BOLD_BRIGHT + "i" + ANSI.YELLOW_BOLD_BRIGHT
              + "l" + ANSI.RESET + ANSI.RED_BOLD_BRIGHT + "d" + " + " + ANSI.YELLOW_BOLD_BRIGHT + "d" + ANSI.RESET
              + ANSI.RED_BOLD_BRIGHT + "r" + ANSI.YELLOW_BOLD_BRIGHT + "a" + ANSI.RESET + ANSI.RED_BOLD_BRIGHT + "w" + " "
              + ANSI.YELLOW_BOLD_BRIGHT + "f" + ANSI.RESET + ANSI.RED_BOLD_BRIGHT + "o" + ANSI.YELLOW_BOLD_BRIGHT
              + "u" + ANSI.RESET + ANSI.RED_BOLD_BRIGHT + "r";
      case R -> ANSI.PURPLE_BOLD + "reverse direction";
    } + ANSI.RESET;
  }

  public void clear() {
    System.out.print("\033[H\033[2J");
    System.out.flush();
  }
}
